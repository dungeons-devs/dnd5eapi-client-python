#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from pydantic import BaseModel

from common import APIReference


class AbilityScore(BaseModel):

    # Resource index for shorthand searching.
    index: str

    # Name of the referenced resource.
    name: str

    # URL of the referenced resource.
    url: str

    # Description of the resource.
    desc: list[str]

    # Full name of the ability score.
    full_name: str

    # List of skills that use this ability score.
    skills: list[APIReference]
