#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from pydantic import BaseModel, validator

from common import APIReference, Choice


class Prerequisite(BaseModel):
    """helper"""

    type: str

    @validator('type')
    def type_values(cls, v):
        permitted = ['level', 'feature', 'spell']
        if v not in permitted:
            raise ValueError('must be a permitted value')
        return v


class LevelPrerequisite(Prerequisite):
    """helper"""

    level: int


class FeaturePrerequisite(Prerequisite):
    """helper"""

    feature: str


class SpellPrerequisite(Prerequisite):
    """helper"""

    spell: str


class FeatureSpecific(BaseModel):
    """helper"""

    subfeature_options: Choice
    expertise_options: Choice
    invocations: list[APIReference]


class Feature(BaseModel):
    class_: APIReference
    desc: list[str]
    parent: APIReference
    index: str
    level: int
    name: str
    prerequisites: list[Prerequisite]
    reference: str
    subclass: APIReference
    feature_specific: FeatureSpecific
    url: str
