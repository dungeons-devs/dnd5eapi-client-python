#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from pydantic import BaseModel

from common import APIReference, AreaOfEffect


class Damage(BaseModel):
    """helper"""

    # From API:
    # As this has keys that are numbers, we have to use an `Object`,
    # which you can't query subfields
    # In API `spell/types.d.ts` it is `Record<number, string>`
    damage_at_slot_level: dict

    # From API:
    # As this has keys that are numbers, we have to use an `Object`,
    # which you can't query subfields
    # In API `spell/types.d.ts` it is `Record<number, string>`
    damage_at_character_level: dict

    damage_type: APIReference


class DC(BaseModel):
    """helper"""

    dc_success: str
    dc_type: APIReference
    desc: str


class Spell(BaseModel):
    area_of_effect: AreaOfEffect
    attack_type: str
    casting_time: str
    classes: list[APIReference]
    components: list[str]
    concentration: bool
    damage: Damage
    dc: DC
    desc: list[str]
    duration: str

    # From API:
    # As this has keys that are numbers, we have to use an `Object`,
    # which you can't query subfields
    # In API `spell/types.d.ts` it is `Record<number, string>`
    heal_at_slot_level: dict

    # In API `spell/index.ts` it is `[String]`
    # In API `spell/types.d.ts` it is `string`
    higher_level: list[str]

    index: str
    level: int
    material: str
    name: str
    range: str
    ritual: bool
    school: APIReference
    subclasses: list[APIReference]
    url: str
