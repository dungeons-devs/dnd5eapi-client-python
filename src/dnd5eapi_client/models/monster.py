#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from typing import Union

from pydantic import BaseModel, validator

from common import APIReference, Choice, Damage, DifficultyClass


class ActionOption(BaseModel):
    """helper"""

    action_name: str
    count: Union[int, str]
    type: str

    @validator('type')
    def type_values(cls, v):
        permitted = ['melee', 'ranged', 'ability', 'magic']
        if v not in permitted:
            raise ValueError('must be a permitted value')
        return v


class ActionUsage(BaseModel):
    """helper"""

    type: str
    dice: str
    min_value: int


class Action(BaseModel):
    """helper"""

    name: str
    desc: str
    attack_bonus: int
    damage: list[Damage]
    dc: DifficultyClass
    usage: ActionUsage
    multiattack_type: str
    action_options: Choice
    actions: list[ActionOption]

    @validator('multiattack_type')
    def multiattack_type_values(cls, v):
        permitted = ['actions', 'action_options']
        if v not in permitted:
            raise ValueError('must be a permitted value')
        return v


class LegendaryAction(BaseModel):
    """helper"""

    name: str
    desc: str
    attack_bonus: int

    # In API `monster/types.d.ts` has `ActionDamage`
    damage: list[Damage]

    dc: DifficultyClass


class Proficiency(BaseModel):
    """helper"""

    proficiency: APIReference
    value: int


class Reaction(BaseModel):
    """helper"""

    name: str
    desc: str
    dc: DifficultyClass


class Sense(BaseModel):
    """helper"""

    blindsight: str
    darkvision: str
    passive_perception: int
    tremorsense: str
    truesight: str


class SpecialAbilityUsage(BaseModel):
    """helper"""

    type: str
    times: int
    rest_types: list[str]


class SpecialAbilitySpell(BaseModel):
    """helper"""

    name: str
    level: int
    url: str
    notes: str
    usage: SpecialAbilityUsage


class SpecialAbilitySpellcasting(BaseModel):
    """helper"""

    level: int
    ability: APIReference
    dc: int
    modifier: int
    components_required: list[str]
    school: str

    # From API comment:
    # As this has keys that are numbers, we have to use an `Object`, which
    # you can't query subfields
    # In API `monster/types.d.ts` has `Record<string, number>`
    slots: dict

    spells: list[SpecialAbilitySpell]


class SpecialAbility(BaseModel):
    """helper"""

    name: str
    desc: str
    attack_bonus: int
    damage: list[Damage]
    dc: DifficultyClass
    spellcasting: SpecialAbilitySpellcasting
    usage: SpecialAbilityUsage


class Speed(BaseModel):
    """helper"""

    burrow: str
    climb: str
    fly: str

    # In API `monster/types.d.ts` has `string`
    hover: bool

    swim: str
    walk: str


class Monster(BaseModel):
    actions: list[Action]
    alignment: str
    armor_class: int
    challenge_rating: int
    charisma: int
    condition_immunities: list[APIReference]
    constitution: int
    damage_immunities: list[str]
    damage_resistances: list[str]
    damage_vulnerabilities: list[str]
    dexterity: int
    forms: list[APIReference]
    hit_dice: str
    hit_points: int
    hit_points_roll: str
    image: str
    index: str
    intelligence: int
    languages: str
    legendary_actions: list[LegendaryAction]
    name: str
    proficiencies: list[Proficiency]
    reactions: list[Reaction]
    senses: Sense
    size: str
    special_abilities: list[SpecialAbility]
    speed: Speed
    strength: int
    subtype: str
    type: str
    url: str
    wisdom: int
    xp: int
