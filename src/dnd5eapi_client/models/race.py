#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from pydantic import BaseModel

from common import APIReference, Choice


class RaceAbilityBonus(BaseModel):
    """helper"""

    ability_score: APIReference
    bonus: int


class Race(BaseModel):
    ability_bonus_options: Choice
    ability_bonuses: list[RaceAbilityBonus]
    age: str
    alignment: str
    index: str
    language_desc: str
    language_options: Choice
    languages: list[APIReference]
    name: str
    size: str
    size_description: str
    speed: int
    starting_proficiencies: list[APIReference]
    starting_proficiency_options: Choice
    subraces: list[APIReference]
    traits: list[APIReference]
    url: str
