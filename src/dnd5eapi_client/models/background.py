#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from pydantic import BaseModel

from common import APIReference, Choice


class Equipment(BaseModel):
    """helper"""

    equipment: APIReference
    quantity: int


class Feature(BaseModel):
    """helper"""

    name: str
    desc: list[str]


class Background(BaseModel):

    # Resource index for shorthand searching.
    index: str
    
    # Name of the referenced resource.
    name: str

    # URL of the referenced resource.
    url: str

    # Starting proficiencies for all new characters of this background.
    starting_proficiencies: list[APIReference]

    #  Starting equipment for all new characters of this background.
    starting_equipment: list[Equipment]


    starting_equipment_options: list[Choice]


    language_options: Choice


    feature: Feature


    personality_traits: Choice


    ideals: Choice


    bonds: Choice


    flaws: Choice
