#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from pydantic import BaseModel

from common import APIReference, Choice


class Equipment(BaseModel):
    """helper"""

    equipment: APIReference
    quantity: int


class StartingEquipmentOption(BaseModel):
    """helper"""

    equipment: APIReference
    quantity: int


class SpellcastingInfo(BaseModel):
    """helper"""

    desc: list[str]
    name: str


class Spellcasting(BaseModel):
    """helper"""

    info: list[SpellcastingInfo]
    level: int
    spellcasting_ability: APIReference


class MultiClassingPrereq(BaseModel):
    """helper"""

    ability_score: APIReference
    minimum_score: int


class MultiClassing(BaseModel):
    """helper"""

    prerequisites: list[MultiClassingPrereq]
    prerequisite_options: Choice
    proficiencies: list[APIReference]
    proficiency_choices: list[Choice]


class Class(BaseModel):
    class_levels: str
    multi_classing: MultiClassing
    hit_die: int
    index: str
    name: str
    proficiencies: list[APIReference]
    proficiency_choices: list[Choice]
    saving_throws: list[APIReference]
    spellcasting: Spellcasting
    spells: str
    starting_equipment: list[Equipment]
    starting_equipment_options: list[Choice]
    subclasses: list[APIReference]
    url: str
