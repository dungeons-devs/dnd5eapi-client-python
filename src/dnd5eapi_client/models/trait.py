#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from pydantic import BaseModel

from common import APIReference, AreaOfEffect, Choice, DifficultyClass


class Proficiency(BaseModel):
    """helper"""

    index: str
    name: str
    url: str


class ActionDamage(BaseModel):
    """helper"""

    damage_type: APIReference

    # In API:
    # As this has keys that are numbers, we have to use an `Object`,
    # which you can't query subfields
    # In API `trait/types.d.ts` it is `Record<string, string>;`
    damage_at_character_level: dict


class Usage(BaseModel):
    """helper"""

    type: str
    times: int


class Action(BaseModel):
    """helper"""

    name: str
    desc: str
    usage: Usage
    dc: DifficultyClass
    damage: list[ActionDamage]
    area_of_effect: AreaOfEffect


class TraitSpecific(BaseModel):
    """helper"""

    subtrait_options: Choice
    spell_options: Choice
    damage_type: APIReference
    breath_weapon: Action


class Trait(BaseModel):
    desc: list[str]
    index: str
    name: str
    proficiencies: list[Proficiency]
    proficiency_choices: Choice
    language_options: Choice
    races: list[APIReference]
    subraces: list[APIReference]
    parent: APIReference
    trait_specific: TraitSpecific
    url: str
