#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from pydantic import BaseModel

from common import APIReference


class ArmorClass(BaseModel):
    """helper"""

    base: int
    dex_bonus: bool
    max_bonus: int


class Content(BaseModel):
    """helper"""

    item: APIReference
    quantity: int


class Cost(BaseModel):
    """helper"""

    quantity: int
    unit: str


class Damage(BaseModel):
    """helper"""

    damage_dice: str
    damage_type: APIReference


class Range(BaseModel):
    """helper"""

    long: int
    normal: int


class Speed(BaseModel):
    """helper"""

    quantity: int
    unit: str


class ThrowRange(BaseModel):
    """helper"""

    long: int
    normal: int


class TwoHandedDamage(BaseModel):
    """helper"""

    damage_dice: str
    damage_type: APIReference


class Equipment(BaseModel):
    armor_category: str
    armor_class: ArmorClass
    capacity: str
    category_range: str
    contents: list[Content]
    cost: Cost
    damage: Damage
    desc: list[str]
    equipment_category: APIReference
    gear_category: APIReference
    index: str
    name: str
    properties: list[APIReference]
    quantity: int
    range: Range
    special: list[str]
    speed: Speed
    stealth_disadvantage: bool
    str_minimum: int
    throw_range: ThrowRange
    tool_category: str
    two_handed_damage: TwoHandedDamage
    url: str
    vehicle_category: str
    weapon_category: str
    weapon_range: str
    weight: int
