#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from typing import Union

from pydantic import BaseModel, validator


class APIReference(BaseModel):

    # Resource index for shorthand searching.
    index: str

    # Name of the referenced resource.
    name: str

    # URL of the referenced resource.
    url: str


class AreaOfEffect(BaseModel):
    size: int
    type: str

    @validator('type')
    def type_values(cls, v):
        permitted = ['sphere', 'cube', 'cylinder', 'line', 'cone']
        if v not in permitted:
            raise ValueError('must be a permitted value')
        return v


class DifficultyClass(BaseModel):
    dc_type: APIReference
    dc_value: int
    success_type: str

    @validator('success_type')
    def success_type_values(cls, v):
        permitted = ['none', 'half', 'other']
        if v not in permitted:
            raise ValueError('must be a permitted value')
        return v


class Damage(BaseModel):
    damage_type: APIReference
    damage_dice: str


class Option(BaseModel):
    """helper"""

    option_type: str

    @validator('option_type')
    def option_type_values(cls, v):
        permitted = ['reference', 'action', 'multiple', 'string',
                     'ideal', 'counted_reference', 'score_prerequisite',
                     'ability_bonus', 'breath', 'damage']
        if v not in permitted:
            raise ValueError('must be a permitted value')
        return v


class ReferenceOption(Option):
    """helper"""

    item: APIReference


class ActionOption(Option):
    """helper"""

    action_name: str
    count: Union[int, str]
    type: str
    notes: str

    @validator('type')
    def type_values(cls, v):
        permitted = ['melee', 'ranged', 'ability', 'magic']
        if v not in permitted:
            raise ValueError('must be a permitted value')
        return v


class MultipleOption(Option):
    """helper"""

    items: list[Option]


class StringOption(Option):
    """helper"""

    string: str


class IdealOption(Option):
    """helper"""

    desc: str
    alignments: list[APIReference]


class CountedReferenceOption(Option):
    """helper"""

    count: int
    of: APIReference

    # ### Only in common/types.d.ts of API definition
    # prerequisites?: {
    #   type: 'proficiency'
    #   proficiency?: APIReference
    # }[]


class ScorePrerequisiteOption(Option):
    """helper"""

    ability_score: APIReference
    minimum_score: int


class AbilityBonusOption(Option):
    """helper"""

    ability_score: APIReference
    bonus: int


class BreathOption(Option):
    """helper"""

    name: str
    dc: DifficultyClass
    damage: list[Damage]


class DamageOption(Option):
    """helper"""

    damage_type: APIReference
    damage_dice: str
    notes: str


class OptionSet(BaseModel):
    option_set_type: str

    @validator('option_set_type')
    def option_set_type_values(cls, v):
        permitted = ['equipment_category', 'resource_list', 'options_array']
        if v not in permitted:
            raise ValueError('must be a permitted value')
        return v


class EquipmentCategoryOptionSet(OptionSet):
    """helper"""

    equipment_category: APIReference


class ResourceListOptionSet(OptionSet):
    """helper"""

    resource_list_url: str


class OptionsArrayOptionSet(OptionSet):
    """helper"""

    options: list[Option]


class Choice(BaseModel):
    
    # Description of the choice to be made.
    desc: str

    # Number of items to pick from the list.
    choose: int

    # Type of the resources to choose from.
    type: str

    # Option Set
    from_: OptionSet


class ChoiceOption(Option):
    """helper"""

    choice: Choice
