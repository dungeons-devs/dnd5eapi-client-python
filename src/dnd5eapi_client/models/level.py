#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from pydantic import BaseModel

from common import APIReference


class ClassSpecificCreatingSpellSlot(BaseModel):
    """helper"""

    sorcery_point_cost: int
    spell_slot_level: int


class ClassSpecificMartialArt(BaseModel):
    """helper"""

    dice_count: int
    dice_value: int


class ClassSpecificSneakAttack(BaseModel):
    """helper"""

    dice_count: int
    dice_value: int


class ClassSpecific(BaseModel):
    """helper"""

    action_surges: int
    arcane_recovery_levels: int
    aura_range: int
    bardic_inspiration_die: int
    brutal_critical_dice: int
    channel_divinity_charges: int
    creating_spell_slots: list[ClassSpecificCreatingSpellSlot]
    destroy_undead_cr: int
    extra_attacks: int
    favored_enemies: int
    favored_terrain: int
    indomitable_uses: int
    invocations_known: int
    ki_points: int
    magical_secrets_max_5: int
    magical_secrets_max_7: int
    magical_secrets_max_9: int
    martial_arts: ClassSpecificMartialArt
    metamagic_known: int
    mystic_arcanum_level_6: int
    mystic_arcanum_level_7: int
    mystic_arcanum_level_8: int
    mystic_arcanum_level_9: int
    rage_count: int
    rage_damage_bonus: int
    sneak_attack: ClassSpecificSneakAttack
    song_of_rest_die: int
    sorcery_points: int
    unarmored_movement: int
    wild_shape_fly: bool
    wild_shape_max_cr: int
    wild_shape_swim: bool


class Spellcasting(BaseModel):
    """helper"""

    cantrips_known: int
    spell_slots_level_1: int
    spell_slots_level_2: int
    spell_slots_level_3: int
    spell_slots_level_4: int
    spell_slots_level_5: int
    spell_slots_level_6: int
    spell_slots_level_7: int
    spell_slots_level_8: int
    spell_slots_level_9: int
    spells_known: int


class SubclassSpecific(BaseModel):
    """helper"""

    additional_magical_secrets_max_lvl: int
    aura_range: int


class Level(BaseModel):
    ability_score_bonuses: int
    class_: APIReference
    class_specific: ClassSpecific
    features: list[APIReference]
    index: str
    level: int
    prof_bonus: int
    spellcasting: Spellcasting
    subclass: APIReference
    subclass_specific: SubclassSpecific
    url: str
