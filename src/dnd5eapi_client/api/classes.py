#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from enum import Enum

from ..models.class_ import Class, _Spellcasting, _MultiClassing


class ClassIndex(Enum):
    BARBARIAN = 'barbarian'
    BARD = 'bard'
    CLERIC = 'cleric'
    DRUID = 'druid'
    FIGHTER = 'fighter'
    MONK = 'monk'
    PALADIN = 'paladin'
    RANGER = 'ranger'
    ROGUE = 'rogue'
    SORCERER = 'sorcerer'
    WARLOCK = 'warlock'
    WIZARD = 'wizard'


def class_(index: ClassIndex) -> Class:
    """
    Get a class by index.

    GET /api/classes/{index}

    Class
    A character class is a fundamental part of the identity and nature of
    characters in the Dungeons & Dragons role-playing game. A character's
    capabilities, strengths, and weaknesses are largely defined by its class.
    A character's class affects a character's available skills and abilities.
    [[SRD p8-55](https://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf#page=8)]

    REQUEST
    Authentication: Not Required

    PATH PARAMETERS
    * index - enum - The `index` of the class to get.

    RESPONSE: 200 OK application/json
    SCHEMA: `Class`
    """

    path = f'/api/classes/{index}'
    data = {}

    return Class(**data)


def spellcasting(index: ClassIndex) -> _Spellcasting:
    """
    Get spellcasting info for a class.

    GET /api/classes/{index}/spellcasting

    REQUEST
    Authentication: Not Required

    PATH PARAMETERS
    * index - enum - The `index` of the class to get.

    RESPONSE:
    200 OK application/json
        SCHEMA: `Spellcasting`
    404 Not found.
        ```application/json
        object
        {
            error*: string
        }
        ```
    """

    path = f'/api/classes/{index}/spellcasting'
    data = {}

    return _Spellcasting(**data)


def multiclassing(index: ClassIndex) -> _MultiClassing:
    """
    Get multiclassing resource for a class.

    GET /api/classes/{index}/multi-classing

    REQUEST
    Authentication: Not Required

    PATH PARAMETERS
    * index - enum - The `index` of the class to get.

    RESPONSE: 200 OK application/json
    SCHEMA: `Multiclassing`
    """

    path = f'GET /api/classes/{index}/multi-classing'
    data = {}

    return _MultiClassing(**data)
