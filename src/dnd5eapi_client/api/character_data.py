#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from enum import Enum

from ..models.ability_score import AbilityScore
from ..models.alignment import Alignment
from ..models.background import Background
from ..models.language import Language
from ..models.proficiency import Proficiency
from ..models.skill import Skill


class AbilityScoresIndex(Enum):
    CHA = 'cha'
    CON = 'con'
    DEX = 'dex'
    INT = 'int'
    STR = 'str'
    WIS = 'wis'


def ability_scores(index: AbilityScoresIndex) -> AbilityScore:
    """
    Get an ability score by index.

    GET /api/ability-scores/{index}

    Ability Score
    Represents one of the six abilities that describes a creature's physical
    and mental characteristics. The three main rolls of the game - the ability
    check, the saving throw, and the attack roll - rely on the ability scores.
    [[SRD p76](https://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf#page=76)]

    REQUEST
    Authentication: Not Required

    PATH PARAMETERS
    * index - enum - The `index` of the ability score to get.

    RESPONSE: 200 OK application/json
    SCHEMA: `AbilityScore`
    """

    path = f'/api/ability-scores/{index}'
    data = {}

    return AbilityScore(**data)


class AlignmentsIndex(Enum):
    CHAOTIC_NEUTRAL = ' chaotic-neutral'
    CHAOTIC_EVIL = 'chaotic-evil'
    CHAOTIC_GOOD = 'chaotic-good'
    LAWFUL_NEUTRAL = 'lawful-neutral'
    LAWFUL_EVIL = 'lawful-evil'
    LAWFUL_GOOD = 'lawful-good'
    NEUTRAL = 'neutral'
    NEUTRAL_EVIL = 'neutral-evil'
    NEUTRAL_GOOD = 'neutral-good'


def alignments(index: AlignmentsIndex) -> Alignment:
    """
    Get an alignment by index.

    GET /api/alignments/{index}

    Alignment
    A typical creature in the game world has an alignment, which broadly
    describes its moral and personal attitudes. Alignment is a combination
    of two factors: one identifies morality (good, evil, or neutral), and the
    other describes attitudes toward society and order (lawful, chaotic, or
    neutral). Thus, nine distinct alignments define the possible combinations.
    [[SRD p58](https://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf#page=58)]

    REQUEST
    Authentication: Not Required

    PATH PARAMETERS
    * index - enum - The `index` of the alignment to get.

    RESPONSE: 200 OK application/json
    SCHEMA: `Alignment`
    """

    path = f'/api/alignments/{index}'
    data = {}

    return Alignment(**data)


class BackgroundIndex(Enum):
    ACOLYTE = 'acolyte'


def background(index: BackgroundIndex) -> Background:
    """
    Get a background by index.

    GET /api/backgrounds/{index}

    Background
    Every story has a beginning. Your character's background reveals where
    you came from, how you became an adventurer, and your place in the world.
    Choosing a background provides you with important story cues about your
    character's identity.
    [[SRD p60](https://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf#page=60)]

    *Note:* acolyte is the only background included in the SRD.

    REQUEST
    Authentication: Not Required

    PATH PARAMETERS
    * index - enum - The `index` of the background to get.

    RESPONSE: 200 OK application/json
    SCHEMA: `Background`
    """

    path = f'/api/backgrounds/{index}'
    data = {}

    return Background(**data)


class LanguageIndex(Enum):
    ABYSSAL = 'abyssal'
    CELESTIAL = 'celestial'
    COMMON = 'common'
    DEEP_SPEECH = 'deep-speech'
    DRACONIC = 'draconic'
    DWARVISH = 'dwarvish'
    ELVISH = 'elvish'
    GIANT = 'giant'
    GNOMISH = 'gnomish'
    GOBLIN = 'goblin'
    HALFLING = 'halfling'
    INFERNAL = 'infernal'
    ORC = 'orc'
    PRIMORDIAL = 'primordial'
    SYLVAN = 'sylvan'
    UNDERCOMMON = 'undercommon'


def language(index: LanguageIndex) -> Language:
    """
    Get a language by index.

    GET /api/languages/{index}

    Language
    Your race indicates the languages your character can speak by default,
    and your background might give you access to one or more additional
    languages of your choice.
    [[SRD p59](https://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf#page=59)]

    REQUEST
    Authentication: Not Required

    PATH PARAMETERS
    * index - enum - The `index` of the language to get.

    RESPONSE: 200 OK application/json
    SCHEMA: `Language`
    """

    path = f'/api/languages/{index}'
    data = {}

    return Language(**data)


def proficiency(index: str):
    """
    Get a proficiency by index.

    GET /api/proficiencies/{index}

    Proficiency
    By virtue of race, class, and background a character is proficient at
    using certain skills, weapons, and equipment. Characters can also gain
    additional proficiencies at higher levels or by multiclassing. A
    characters starting proficiencies are determined during character creation.

    REQUEST
    Authentication: Not Required

    PATH PARAMETERS
    * index - string - The `index` of the proficiency to get.
    Available values can be found in the ResourceList for proficiencies.

    RESPONSE: 200 OK application/json
    SCHEMA: `Proficiency`
    """

    path = f'/api/proficiencies/{index}'
    data = {}

    return Proficiency(**data)


class SkillIndex(Enum):
    ACROBATICS = 'acrobatics'
    ANIMAL_HANDLING = 'animal-handling'
    ARCANA = 'arcana'
    ATHLETICS = 'athletics'
    DECEPTION = 'deception'
    HISTORY = 'history'
    INSIGHT = 'insight'
    INTIMIDATION = 'intimidation'
    INVESTIGATION = 'investigation'
    MEDICINE = 'medicine'
    NATURE = 'nature'
    PERCEPTION = 'perception'
    PERFORMANCE = 'performance'
    PERSUASION = 'persuasion'
    RELIGION = 'religion'
    SLEIGHT_OF_HAND = 'sleight-of-hand'
    STEALTH = 'stealth'
    SURVIVAL = 'survival'


def skill(index: SkillIndex) -> Skill:
    """
    Get a skill by index.

    GET /api/skills/{index}

    Skill
    Each ability covers a broad range of capabilities, including skills
    that a character or a monster can be proficient in. A skill represents
    a specific aspect of an ability score, and an individual's proficiency
    in a skill demonstrates a focus on that aspect.
    [[SRD p77](https://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf#page=77)]

    REQUEST
    Authentication: Not Required

    PATH PARAMETERS
    * index - enum - The `index` of the skill to get.

    RESPONSE: 200 OK application/json
    SCHEMA: `Skill`
    """

    path = f'/api/skills/{index}'
    data = {}

    return Skill(**data)
