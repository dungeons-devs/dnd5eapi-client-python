#!/usr/bin/env python3
# -*- coding: utf-8, vim: expandtab:ts=4 -*-

# This file is part of DnD5eAPI-Client-Python.
#
# DnD5eAPI-Client-Python is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DnD5eAPI-Client-Python is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with DnD5eAPI-Client-Python. If not, see <https://www.gnu.org/licenses/>.

from enum import Enum


def index_lv1():
    """
    Get all resource URLs.

    GET /api

    Making a request to the API's base URL returns an object containing
    available endpoints.

    REQUEST
    Authentication: Not Required

    RESPONSE
    200 OK

    SCHEMA
    `Object`
    ```application/json
    {
        <any-key>: string
    }
    ```
    """

    path = '/api'


class ApiEndpointParameter(Enum):
    ABILITY_SCORES = 'ability-scores'
    ALIGNMENTS = 'alignments'
    BACKGROUNDS = 'backgrounds'
    CLASSES = 'classes'
    CONDITIONS = 'conditions'
    DAMAGE_TYPES = 'damage-types'
    EQUIPMENT = 'equipment'
    EQUIPMENT_CATEGORIES = 'equipment-categories'
    FEATS = 'feats'
    FEATURES = 'features'
    LANGUAGES = 'languages'
    MAGIC_ITEMS = 'magic-items'
    MAGIC_SCHOOLS = 'magic-schools'
    MONSTERS = 'monsters'
    PROFICIENCIES = 'proficiencies'
    RACES = 'races'
    RULE_SECTIONS = 'rule-sections'
    RULES = 'rules'
    SKILLS = 'skills'
    SPELLS = 'spells'
    SUBCLASSES = 'subclasses'
    SUBRACES = 'subraces'
    TRAITS = 'traits'
    WEAPON_PROPERTIES = 'weapon-properties'


def index_lv2(endpoint: ApiEndpointParameter):
    """
    Get list of all available resources for an endpoint.

    GET /api/{endpoint}

    Currently only the `/spells` and `/monsters` endpoints support filtering
    with query parameters. Use of these query parameters is documented
    under the respective [Spells](https://www.dnd5eapi.co/docs/#tag--Spells)
    and [Monsters](https://www.dnd5eapi.co/docs/#tag--Monsters) sections.

    REQUEST
    Authentication: Not Required

    PATH PARAMETERS
    * endpoint - enum

    RESPONSE
    200 OK

    SCHEMA
    `APIReferenceList`
    ```application/json
    {
        count: number      # Total number of resources available.
        results: [{        # ⮕ [ `APIReference` ]
            index: string  # Resource index for shorthand searching.
            name: string   # Name of the referenced resource.
            url: string    # URL of the referenced resource.
        }]
    }
    ```
    """

    path = f'/api/{endpoint}'
