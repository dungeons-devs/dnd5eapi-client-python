# DnD 5th Edition API - Python client

A Python client to interact with the D&D 5th Edition API ([homepage](https://www.dnd5eapi.co)).

## License

![GPLv3 logo](https://www.gnu.org/graphics/gplv3-127x51.png)

[GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.html)

```
This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License, version 3, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>. 
```
